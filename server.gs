
/* The script is deployed as a web app and renders the form */
function doGet(e) {
  return HtmlService.createTemplateFromFile('form.html')
    .evaluate() // evaluate MUST come before setting the NATIVE mode
    .setTitle('Hawkathon 2016 Application')
    .setSandboxMode(HtmlService.SandboxMode.NATIVE);
    // This is important as form hidden in IFRAME mode.
}

function uploadFiles(form) {
  
  try {
    
    /* ID's of folder to write resumes to and spreadsheet to write application to. */
    var spreadsheet = SpreadsheetApp.openById("ID HERE");
    var folder = DriveApp.getFolderById("ID HERE");
    
    var blob = form.myFile; 
    /* Do some checks on the submitted file to see if we will actually accept the application. */
    if ( blob.getContentType() != "application/pdf" ) {
      return "bad"; 
    }
    
    blob.setName(form.myFirstName + " " + form.myLastName + " Resume " + form.mySchool);
    var file = folder.createFile(blob);    
    file.setDescription("Uploaded by " + form.myFirstName);
 
    var d = new Date();
    var submission_date = d.toDateString();
    /* Wacko URL split below because Google was re-writing URL to wrong johnkilgo.com GApps address. */
    var submission_url = "https://" + "drive" + ".google.com/" + "file/d/" + file.getId() + "/view";
    
    /* Creates the actual row of data with information on the Application spreadsheet. */
    spreadsheet.appendRow([submission_date, form.myFirstName, form.myLastName, form.myHackathon, form.myHack, form.myAge, form.mySchool, form.myYOG, form.myMajor, form.myOtherMajor, form.myEmail, form.myCareer, form.myGit, form.myWebsite, form.myAllergies, submission_url]);
        
    return "good";
    
  } catch (error) {
    
    return error.toString();
  }
  
}