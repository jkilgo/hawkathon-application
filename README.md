# README #

Google apps scripts: HTML form with file submission; data put in spreadsheet.

### How to use ###

* Make a new google apps script on https://www.google.com/script/start/ (Hosts on https://scripts.google.com/)
* Create the two files in this repository within the script console
* Get the ID of both a spreadsheet and a folder in Google Drive that your account can modify
* Go to Publish --> Deploy as Web App...
* Configure options:
    * Execute App as yourself
    * Allow Anyone, even Anonymous to access the app

### Issues ###

1. Form HTML is messy due to the way the Google API handles the file (so I'm led to believe, didn't spend time trying to track down why)
1. Relies on external images / js scripts (be aware if you use)
1. Some of the code for the functions is a little hackish; this wasn't meant to be a mint project, just to get something running and working

### Future ideas ###

1. Switch all DOM manipulations to jquery since we're already using it for validation
1. Clean up the functions a bit
1. Refactor the HTML to try to improve form functionality